#!/usr/bin/env bash
DIR=$( cd `dirname $0` && pwd )
cd $DIR

#List the submodules here
echo "Initiating submodules in $DIR"
MODULES="\
    BAG2_environment_settings \
    BAG2_framework \
    BAG2_methods \
    BAG2_opensource_db \
    BAG2_technology_definition \
    BAG2_technology_template \
    BAG2_templates \
    bag_ecd \
    inverter_gen \
         "
git submodule sync

#Self-made selective recursion
for mod in $MODULES; do
    git submodule update --init
    if [ -f $mod/init_submodules.sh ]; then
        cd $mod
        ./init_submodules.sh
    fi
done

exit 0
