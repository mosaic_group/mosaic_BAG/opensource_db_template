- Check the version of python(must be > 3.10)
- Run "sudo apt install python3-pip -y" to install the pip tool
- Run "sudo apt install build-essential libssl-dev libffi-dev python3-dev" to install the neccesary basic packages
- Run "sudo apt install python3-venv -y"
- "
    mkdir virtual_env
    cd virtual_env
    python3 -m venv <virtual_env_name>
    source ./<virtual_env_name>/bin/activate

  "
- To deactivate, simply type "deactivate"
