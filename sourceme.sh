#! /bin/bash
#
# Sourceme template for a BAG2 project
# ------------------------------------
#
#   This script
#       - defines all the environmental variables required to run BAG2
#       - checks for dependencies
#
#   Usage: source sourceme.sh
#

# -------------------------------------------------------------------------------------------------------

DIR=$(realpath $(dirname -- ${BASH_SOURCE}))
source ${DIR}/BAG2_environment_settings/bash-utils.sh    # for functions: check_tool_available, log_error

function validate_prerequisites() {
    local FOUND_ERROR=0
    (
        #                    TOOL     VERSION COMMAND                         >VERSION           VERSION_BLACKLIST_REGEX
        # ---------------------------------------------------------------------------------------------------------------
        check_tool_available klayout  "klayout -v"                            "KLayout 0.28.8"   ""
        check_tool_available xschem   "xschem -v | grep XSCHEM | head -n 1"   "XSCHEM V3.4.3"    ""
    ) || FOUND_ERROR=1

    if [ "$PWD" = "$HOME" ]; then
      log_error "ERROR: Do not run this script in your home directory."
      echo "Configuration files in home directory may mess up your configuration globally."
      FOUND_ERROR=1
    fi

    if [ "$PDK_ROOT" = "" ]; then
      log_error "ERROR: PDK_ROOT not set"
      echo "In this file you MUST set your environment setup so that you have all tools available and PDK set up correctly"
      echo "It is NOT taken care for you by this file"
      FOUND_ERROR=1
    fi

    return $FOUND_ERROR
}

# -------------------------------------------------------------------------------------------------------

if validate_prerequisites; then
    export TECHLIB=sky130_fd_pr
    export PDK=sky130A  # the variant of the PDK that we chose
    export KLAYOUT_HOME=${PDK_ROOT}/${PDK}/libs.tech/klayout/

    # ngspice specific values:
    export SPICE_SCRIPTS=${PDK_ROOT}/${PDK}/libs.tech/ngspice/   # ngspice loads spinit from here
    export NGSPICE_INPUT_DIR=$SPICE_SCRIPTS   # ngspice will search *.{lib|cir|inc} here
    
    if [ "$USER_HOME_DIR" = "" ]; then
        export USER_HOME_DIR="$DIR"  # NOTE: in case of custom or multi-user environments,
                                     #       set this to different location in the outer shell
    fi

    # setup GEN tool
    export GEN_TOOL_CONFIG_PATH=${DIR}/gentool_config.yaml # include mosaic_opensource_gen generators
    unalias -a gen
    alias gen=${DIR}/gen

    # can be --finfet --flipwell:   # NOTE: no longer used for gen-tool based flow
    export BUILDOPTS=

    #############################################################################
    # NOTE: BAG_WORK_DIR, VIRTUOSO_DIR and BAG_GENERATOR_ROOT
    # are decoupled for flexible definition of locations
    # set BAG root directory
    #############################################################################

    # Defaults to virtuoso_template root
    export BAG_WORK_DIR=${DIR}

    # Virtuoso run directory from which you launch the builds
    # containing "working" artefacts, such as log files and the BAG_server_port.txt, etc)
    export VIRTUOSO_DIR="$BAG_WORK_DIR"

    # Root directory containing generators
    export BAG_GENERATOR_ROOT=${BAG_WORK_DIR}

    # set BAG framework directory
    export BAG_FRAMEWORK="${BAG_WORK_DIR}/BAG2_framework"

    # set BAG python executable
    export BAG_PYTHON="python3"

    # set jupyter notebook path
    #export BAG_JUPYTER "${HOME}/.local/bin/jupyter-notebook"

    # set technology/project configuration directory
    export BAG_TECH_CONFIG_DIR="${BAG_WORK_DIR}/BAG2_technology_definition"

    # set where BAG saves temporary files
    export BAG_TEMP_DIR="${VIRTUOSO_DIR}/BAGTMP"

    # set location of BAG configuration file
    export BAG_CONFIG_PATH="${BAG_WORK_DIR}/bag_config.yaml"

    ## set IPython configuration directory
    ##export IPYTHONDIR "$BAG_WORK_DIR/.ipython"

    # This takes ecd_bag helpers into use
    if [ "$PYTHONPATH" = "" ]; then
        export PYTHONPATH="${BAG_WORK_DIR}:${BAG_WORK_DIR}/bag_ecd:${BAG_WORK_DIR}/BAG2_methods:${BAG_WORK_DIR}/BAG2_framework:${BAG_WORK_DIR}/BAG2_opensource_db"
    else
        export PYTHONPATH="${PYTHONPATH}:${BAG_WORK_DIR}:${BAG_WORK_DIR}/bag_ecd:${BAG_WORK_DIR}/BAG2_methods:${BAG_WORK_DIR}/BAG2_framework:${BAG_WORK_DIR}/BAG2_opensource_db"
    fi

    if [ ! -f ${BAG_GENERATOR_ROOT}/bag_libs.def ]; then
        echo "./bag_libs.def does not exist but is still needed. Creating a dummy file for you."
        echo 'BAG_prim ${BAG_TECH_CONFIG_DIR}/Tech_primitives'  > ${BAG_GENERATOR_ROOT}/bag_libs.def
    fi

    if [ ! -e ${VIRTUOSO_DIR}/BAGTMP ]; then
        mkdir ${VIRTUOSO_DIR}/BAGTMP
    fi

    #
    # TODO: in the future, opensource_db_template should no longer require these Virtuoso related files
    #       for now, as the BAG framework ATM overfits on Virtuoso, the next steps are still necessary...
    #
    # TODO: also, VIRTUOSO_DIR should be perhaps renamed to
    #

    if [ ! -f ${VIRTUOSO_DIR}/cds.lib ]; then
        echo "./cds.lib does not exist but is still needed. Creating a dummy file for you."
        echo "" > ${VIRTUOSO_DIR}/cds.lib
    fi

    if [ ! -e ${VIRTUOSO_DIR}/BAG_server_port.txt ]; then
        echo 0 > ${VIRTUOSO_DIR}/BAG_server_port.txt
    fi
fi
