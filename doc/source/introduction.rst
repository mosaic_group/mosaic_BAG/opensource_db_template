=============================================
Introduction to MOSAIC BAG2 virtuoso template
=============================================

Modular Open Source Analog Integrated Circuits Berkeley Analog Generator
is a development fork of original BAG2 circuit design environment 
developed at University of California, Berkeley.

Objective of this for is to provide a low entry threshod to BAG2 by 
providing a documented easy-to-setup minimum working example of a 
BAG2 desing project. The template id build onthe following scripts and modules

Scripts
-------
In order of execution after cloning

#. init_submodules.sh 

   * initializes all the submoudles of the project


#. sourceme.csh

   * C-shell to be sourced to set up your desing environment the pre-requisite is that you have a *working* virtuoso environment set up before you source this script.


#. configure

   * script that is used to generate a Makefile. The philosophy of *every*

   * compilation is to  run `configure && make <recipe>`



Submodules
----------

Submodules that are documented separately are listed below. Use of 
submodules makes it possible to separate but still enable the 
development  of thid environment from your projects. Updates to methodology can be propagated to 
projects selectively through submodule updates.

* BAG2_environment settings

   * Used to provide environment specific setting files

* BAG2_framework

   * BAG2 core framework forked form UC Berkeley

* BAG2_templates

   * Submodule containing design templates for AnalogBAse 

* BAG2_tecnology_templates

   * Contains abstract classes for process parameter and method setups for BAG2. These classes are used in BAG2_technology_definition as parent classes to orce the implementation of essential process definition parameters. Parameters are documented in this module.

* bag_ecd

   * A interface module that enables easy-to-use generator framework for circuit generators.  With this methodology, every generator has an identical structure of main class, schematic class and layout class, and by following this strucuture all the generators are compatible and can be used in constructions of other generators.

* inverter_gen

   * Minimum working example of an circuit generator.


