
# BAG2 Workspace (Open Source DB) 

## Introduction

The [original BAG2 workspace](https://gitlab.com/mosaic_group/mosaic_BAG/virtuoso_template) requires Cadence Virtuoso.

This alternative workspace is a proof of concept, where Virtuoso (OA database) is substituted by: 
* [XSchem](https://xschem.sourceforge.io) (for schematic entry and parsing)
* [KLayout](https://klayout.de) (for layout generation)

## Status

Currently it implements:
* A single generator `inverter1_gen`
* XSchem template, symbols
* Sky130 technology definitions (under construction)
* LVS 
* DRC

Missing/Pending tasks:
* BAG_prim has to be completed 
  * currently only some cells are existent: `nmos4_lvt` / `nmos4_standard` / `pmos4_lvt` / `pmos4_standard`
* Simulation support
* RCX
* Additional generators (symbols and template schematic have to be converted from OA to xschem)
* Virtuoso is not required, yet in some places Virtuso concepts still are present. Removing them is TODO.

## Instructions For Running 

### Requirements

* Linux (tested with Ubuntu 22.04)
* Python 3.10 and python3-pip
* Git
* Xschem >= 3.4.4
  * see build instructions below
* KLayout >= 0.28.9
  * current version 0.28.9 tested successfully
  * Note: Ruby feature necessary for LVS / DRC
* NGSpice 41

Make sure you have set up the environmental variable `PATH` 
to be able to find `xschem` and `klayout` binaries.

### Python Package Installation

Install the required Python packages:
```
pip3 install klayout gdsfactory volare
```

_NOTE:_ the klayout pip package will only install the klayout bindings (but not klayout itself).

### PDK Installation

In the last step we already installed the PDK package manager `volare`.

Create a directory outside of the `opensource_db_template` to contain the PDK,
the variable `PDK_ROOT` will point to this directory:
```
export PDK_ROOT=$HOME/pdk  # or some other path
export PDK=sky130A   # the variant of the PDK that we chose
export KLAYOUT_HOME=${PDK_ROOT}/${PDK}/libs.tech/klayout/
mkdir $PDK_ROOT
```

Using the package manager `volare` to list to available PDK versions (along with their version hashes): 
```
volare ls-remote --pdk sky130
```

Fetch and install the PDK using the chosen version hash:
```
# NOTE: example version, chose your version from the output of ls-remote:
export CHOSEN_PDK_VERSION=3df14f84ab167baf757134739bb1d2c5c044849c      

volare enable --pdk sky130 $CHOSEN_PDK_VERSION
```

### PDK fix to speed up ngspice simulations

Simulations are quite slow with the default `${PDK_ROOT}/${PDK}/libs.tech/ngspice/sky130.lib.spice`.

Fortunately, Prof. Harald Pretl provides a Python script, which extracts a fast, 
reduced version for a given simulation corner:
https://github.com/iic-jku/osic-multitool/tree/main#spice-model-file-reducer

```
cd ${PDK_ROOT}/${PDK}/libs.tech/ngspice/
wget https://github.com/iic-jku/osic-multitool/raw/main/iic-spice-model-red.py
python3 iic-spice-model-red.py sky130.lib.spice tt
```

This will produce a SPICE file `sky130.lib.spice.tt.red`,
which we later can include in our simulations using
```
.lib sky130.lib.spice.tt.red tt
```

### Xschem PDK Setup

Ensure the `xschemrc` includes the PDK:
```
cat $HOME/.xschem/xschemrc 
source $env(PDK_ROOT)/$env(PDK)/libs.tech/xschem/xschemrc
```

### KLayout PDK Setup

* Click `Tools > Manage Technologies`

  ![Klayout-pdk-popup-menu](./doc/images/klayout-tools-menu.png)

* Ensure the layermap uses sky130A.lyp file (or particular layermap file of the PDK variant set in $PDK variable)
  
  ![Klayout-pdk-popup-menu](./doc/images/klayout-sky130-setup.png)

* Make sure to select the sky130 pdk

  ![Klayout-pdk-popup-menu](./doc/images/klayout-pdk-popup-menu.png)

### Workspace Setup

Depending on your environment, either SSH or HTTPS can be used to clone the Git Repository:
```
# Variant 1 (HTTPS, preferred and tested)
git clone --recursive https://gitlab.com/mosaic_group/mosaic_BAG/opensource_db_template.git -b master

# Variant 2 (SSH)
git clone --recursive git@gitlab.com:mosaic_group/mosaic_BAG/opensource_db_template.git -b master

cd opensource_db_template
```

Init all GIT submodules:
```
./init_submodules.sh    
```

Init all Python packages required by BAG2:
```
./pip3userinstall.sh
```

Initialize the environmental variables:
```
source sourceme.sh
```

Note: this repository already contains the `inverter1_gen` submodule.
To run the generator (creates layout and schematic), use:
```
gen make inverter1_gen
```

Result files:
* Layout: ```./inverter1_generated_sky130_fd_pr.gds```
* Generated schematic: ```inverter1_generated_sky130_fd_pr/inverter1/inverter1.sch```.
  Use ```xschem --rcfile=xschem/xschemrc ...``` to view it.
* LVS run dir: ```./klayout_run/lvs_run_dir/inverter1_generated_sky130_fd_pr/inverter1/```

To try a more comprehensive example, try out the 2-stage-opamp library of generators following this [README](https://gitlab.com/mosaic_group/mosaic_BAG/generators/two_stage_opamp/readme/-/blob/main/README.md?ref_type=heads).

### Debugging a generator

To debug your generator (we recommend PyCharm), you can use the following settings:
* Script path: `${BAG_WORK_DIR}/BAG2_methods/sal/gentool/tool.py`
* Script arguments: `make inverter1_gen`

This is precisely the same script as if you would do on the command line: `gen make inverter1_gen`.

## Appendix

### Using Anaconda

As mentioned above, besides Klayout / Xschem, a Python 3.10 environment is required.
Given a system-wide Python installation, the above mentioned `pip3userinstall.sh` script can be used to install the required Python packages.

Alternatively, many people (who have to switch between different Python versions) prefer to use Anaconda or Virtualenv.

PyCharm also supports Python Interpreters residing within Anaconda or Virtualenv environments.

We provide a Conda recipe to quickly create a Conda environment: 
[bag2_py3_10_environment.yaml](./CI-helpers/bag2_py3_10_environment.yaml)

Having installed Conda and downloaded the recipe, to create the environment, call
```
conda env create --prefix <path-to-your-conda-env> -f bag2_py3_10_environment.yaml
```
_NOTE:_ Using the `--prefix` option, you can specify a dedicated path where the environment will be setup.

### Using venv

For using venv, please look at the [venv recipe](./venv_recipe.txt)

### Running in Windows Subsystem for Linux

Please have a look at the [WSL recipe](./wsl_recipe.txt) to setup WSL.

### Building XSchem on Ubuntu

Packages

```
sudo apt-get install flex bison libx11-dev tk-dev tcl libxpm-dev
```

Building:

```
git clone https://github.com/StefanSchippers/xschem.git  # -b 3.4.4
cd xschem
./configure
make
make install
```

### Building ngspice

For ngspice to work with `PySpice`, ideally it should be configured including the `--with-ngshared` option:
```
wget https://sourceforge.net/projects/ngspice/files/ng-spice-rework/41/ngspice-41.tar.gz
tar xvfz ngspice-41.tar.gz
cd ngspice-41
./configure --with-x --with-readline=yes --disable-debug --enable-xspice --with-ngshared 
make
make install
```

### Useful Klayout Shortcuts

* Change the Hierarchy Level
  * `+` to increase the hierarchy level 
  * `-` to decrease
* Descend / Ascend:
  * go to level 3 for example (using `+`)
  * left-click a shape in the layout view
  * press `CTRL-D` (to descend)
    * NOTE: here all the other stuff will be dimmed and the related cell will be selected in the cells master list
  * press `CTRL-A` (to ascend) back
* Properties of Shapes / Cells:
  * left-click a shape in the layout view
  * press `Q` to show properties window


### Contributors

Created by (during the SAL Bootcamp 2022) 
* Matthias Koefferlein
* Thomas Parry

Additional contributions by
* Martin Köhler
* Vishal Sivakumar
